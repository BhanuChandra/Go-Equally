﻿using SplitWise.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SplitWise.DataAccessLayer
{
    public class DataAccess_Layer
    {
        static int UserId = 0;
        static int TransactionId = 0;
        SplitWiseContext context;
        public SplitWiseContext Context { get { return context; } }

        public DataAccess_Layer()
        {
            context = new SplitWiseContext();
        }

        //Get all account details

        public List<Account> GetAccountDetails()
        {
            List<Account> account_details;

            try
            {

                account_details = (from r in Context.Account select r).ToList();

            }
            catch (Exception)
            {
                throw;
            }
            return account_details;
        }




        //User Registration
        public bool add_new_Account(Account new_account_obj)
        {
            bool status = false;
            string userId = null;

            List<Account> account_details;

            try
            {
                Account new_account = new Account();
                DateTime datetime = DateTime.Now;
                account_details = (from r in Context.Account select r).ToList();

                //UserId generation in relal time
                UserId = account_details.Count + 1000;
                userId = 'U' + UserId.ToString();

                //Adding new details to DB-object
                new_account.Uname = new_account_obj.Uname;
                new_account.UserId = userId;
                new_account.Email = new_account_obj.Email;
                new_account.Password = new_account_obj.Password;
                new_account.DateDayTime = datetime;

                //Adding the new Object to DBSET
                Context.Account.Add(new_account);

                //Effecting the DB
                int count = Context.SaveChanges();

                if (count != 0)
                    status = true;

            }
            catch (Exception)
            {
                throw;
            }
            return status;
        }

        //User Login
        public bool userLogin(Account Login_Account)
        {
            bool status = false;
            //Console.WriteLine("Before: "+Login_Account.Email);
            //Console.WriteLine("Before: "+Login_Account.Password);
            Account userlogin;

            try
            {
                userlogin = (from r in Context.Account where Login_Account.Email == r.Email && Login_Account.Password == r.Password select r).FirstOrDefault();
                if (userlogin.Email == Login_Account.Email && userlogin.Password == Login_Account.Password)
                {
                    status = true;
                }
                else
                    status = false;
            }
            catch
            {
                status = false;
            }

            return status;
        }

        /* *********************ADMIN SPECIFIC ACTIVITIES*************************** */

        //Add Admin Individual Mmembers
        public int Get_User_Member_Count(UserMemberDetails getcountBY_email)
        {
            int memberCount = 0;
            List<UserMemberDetails> Admin_Members_details;
            try
            {
                Admin_Members_details = (from m in context.UserMemberDetails where m.OwnerEmail == getcountBY_email.OwnerEmail select m).ToList();
                memberCount = Admin_Members_details.Count();
            }
            catch
            {
                throw;
            }

            return memberCount;
        }
        //Add new member in existed account
        public string AddMember(UserMemberDetails addMember)
        {
            string status = "";

            UserMemberDetails new_member = new UserMemberDetails();

            UserMemberDetails member_status = new UserMemberDetails();

            Account check_account_existence = new Account();

            Account getAccountdetails = new Account();

            try
            {
                check_account_existence = (from a in context.Account where a.Email == addMember.MemberEmail select a).FirstOrDefault();


                if (check_account_existence != null)
                {
                    member_status = (from m in context.UserMemberDetails where m.MemberEmail == addMember.MemberEmail && m.OwnerEmail == addMember.OwnerEmail select m).FirstOrDefault();
                    if (member_status == null)
                    {
                        new_member.MemberName = addMember.MemberName;
                        new_member.MemberEmail = addMember.MemberEmail;
                        new_member.OwnerEmail = addMember.OwnerEmail;
                        new_member.DateDayTime = DateTime.Now;
                        new_member.GivenAmount = addMember.GivenAmount;
                        new_member.BarrowedAmount = addMember.BarrowedAmount;

                        Context.UserMemberDetails.Add(new_member);
                        int entries = Context.SaveChanges();

                        if (entries == 1)
                        {
                            member_status = (from m in context.UserMemberDetails where m.OwnerEmail == addMember.MemberEmail && m.MemberEmail == addMember.OwnerEmail select m).FirstOrDefault();
                            if(member_status == null)
                            {
                                getAccountdetails = (from a in context.Account where a.Email == addMember.OwnerEmail select a).FirstOrDefault();
                                new_member.MemberName = getAccountdetails.Uname;
                                new_member.OwnerEmail = addMember.MemberEmail;
                                new_member.MemberEmail = addMember.OwnerEmail;
                                new_member.DateDayTime = DateTime.Now;
                                Context.UserMemberDetails.Add(new_member);
                                Context.SaveChanges();
                            }
                            status = "Success";
                        }

                        else if (entries == 0)
                        {
                            status = "Failed";
                        }
                    }
                    else
                    {
                        status = "Existed";
                    }
                }
                else
                {
                    status = "Member your trying to add is not authorized user";
                }
            }
            catch
            {
                throw;
            }

            return status;
        }

        //Get Transactions by email
        public List<Transactions> MyTransactions(Account account)
        {
            List<Transactions> myMembers = new List<Transactions>();

            myMembers = (from t in context.Transactions where t.OwnerEmail == account.Email select t).ToList();

            return myMembers;
        }

        //Get members by email
        public List<UserMemberDetails> GetMyMembers(Account account)
        {
            List<UserMemberDetails> myMembers = new List<UserMemberDetails>();
            try
            {
                myMembers = (from M in context.UserMemberDetails where M.OwnerEmail == account.Email select M).ToList();
            }
            catch
            {
                throw;
            }
            
            return myMembers;
        }

        public bool DeleteMember(UserMemberDetails delmember)
        {
            bool status = false;
            try
            {
                UserMemberDetails usermember = (from um in context.UserMemberDetails where um.MemberEmail == delmember.MemberEmail && um.OwnerEmail == delmember.OwnerEmail select um).First();
                context.UserMemberDetails.Remove(usermember);
                int a = context.SaveChanges();
                if (a == 1)
                    status = true;
                else
                    status = false;
            }
            catch
            {
                throw;
            }
            return status;
        }
        
        public bool AddTransactions(Transactions transactions)
        {
            bool status = false;
            Transactions new_transactions = new Transactions();
            var transactions_list = (from tr in context.Transactions select tr).ToList();
            TransactionId = transactions_list.Count + 1000;
            var final_transactionId = "STRA" + TransactionId.ToString();
            try
            {
                var check_member = (from mem in context.UserMemberDetails where mem.OwnerEmail == transactions.OwnerEmail && mem.MemberEmail == transactions.MemberEmail select mem).FirstOrDefault();

                new_transactions.TransactionId = final_transactionId;
                new_transactions.MemberEmail = transactions.MemberEmail;
                new_transactions.OwnerEmail = transactions.OwnerEmail;
                new_transactions.PersonName = transactions.PersonName;
                new_transactions.Note = transactions.Note;
                new_transactions.GivenAmount = transactions.GivenAmount;
                new_transactions.BarrowedAmount = transactions.BarrowedAmount;
                new_transactions.DateDayTime = DateTime.Now;
                if(check_member == null)
                {
                    status = false;
                    
                }
                else
                {
                    context.Transactions.Add(new_transactions);
                    var state = context.SaveChanges();
                    if (state == 1)
                    {
                        status = true;
                    }
                    else if (state < 1)
                    {
                        status = false;
                    }
                }
          
            }
            catch
            {
                status = false;
                throw;
            }
            return status;
        }


    }


}





