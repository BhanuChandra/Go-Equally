﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace SplitWise.DataAccessLayer.Models
{
    public partial class SplitWiseContext : DbContext
    {
        public SplitWiseContext()
        {
        }

        public SplitWiseContext(DbContextOptions<SplitWiseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<UserMemberDetails> UserMemberDetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("SplitWiseDBConnectionString");

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(e => e.Email);

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DateDayTime)
                    .HasColumnName("Date_Day_Time")
                    .HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.Uname)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Transactions>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.Property(e => e.TransactionId)
                    .HasColumnName("Transaction_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BarrowedAmount).HasColumnName("Barrowed_Amount");

                entity.Property(e => e.DateDayTime)
                    .HasColumnName("Date_Day_Time")
                    .HasColumnType("datetime");

                entity.Property(e => e.GivenAmount).HasColumnName("Given_Amount");

                entity.Property(e => e.MemberEmail)
                    .HasColumnName("Member_Email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Note)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerEmail)
                    .HasColumnName("Owner_Email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PersonName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.UserMemberDetails)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => new { d.OwnerEmail, d.MemberEmail })
                    .HasConstraintName("FK__Transactions__5CD6CB2B");
            });

            modelBuilder.Entity<UserMemberDetails>(entity =>
            {
                entity.HasKey(e => new { e.OwnerEmail, e.MemberEmail });

                entity.ToTable("User_Member_Details");

                entity.Property(e => e.OwnerEmail)
                    .HasColumnName("Owner_Email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MemberEmail)
                    .HasColumnName("Member_Email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BarrowedAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DateDayTime)
                    .HasColumnName("Date_Day_Time")
                    .HasColumnType("datetime");

                entity.Property(e => e.GivenAmount)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MemberName)
                    .HasColumnName("Member_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
