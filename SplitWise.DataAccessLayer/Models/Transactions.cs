﻿using System;
using System.Collections.Generic;

namespace SplitWise.DataAccessLayer.Models
{
    public partial class Transactions
    {
        public string TransactionId { get; set; }
        public string OwnerEmail { get; set; }
        public string MemberEmail { get; set; }
        public string PersonName { get; set; }
        public string Note { get; set; }
        public long? GivenAmount { get; set; }
        public long? BarrowedAmount { get; set; }
        public DateTime? DateDayTime { get; set; }

        public UserMemberDetails UserMemberDetails { get; set; }
    }
}
