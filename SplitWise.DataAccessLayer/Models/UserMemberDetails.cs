﻿using System;
using System.Collections.Generic;

namespace SplitWise.DataAccessLayer.Models
{
    public partial class UserMemberDetails
    {
        public UserMemberDetails()
        {
            Transactions = new HashSet<Transactions>();
        }

        public string MemberName { get; set; }
        public string OwnerEmail { get; set; }
        public string MemberEmail { get; set; }
        public string BarrowedAmount { get; set; }
        public string GivenAmount { get; set; }
        public DateTime? DateDayTime { get; set; }

        public ICollection<Transactions> Transactions { get; set; }
    }
}
