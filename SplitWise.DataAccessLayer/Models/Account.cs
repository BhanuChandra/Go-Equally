﻿using System;
using System.Collections.Generic;

namespace SplitWise.DataAccessLayer.Models
{
    public partial class Account
    {
        public string Uname { get; set; }
        public string UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime DateDayTime { get; set; }
    }
}
