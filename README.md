# SplitBill

This project helps us to manage our daily spending's and also group spends.


CREATE DATABASE SplitWise

USE SplitWise


GO
CREATE TABLE Account
(
Uname VARCHAR(50) NOT NULL,
UserId VARCHAR(5) NOT NULL,
Email VARCHAR(50) CONSTRAINT pk_account_mail PRIMARY KEY,
[Password] VARCHAR(25) NOT NULL,
Date_Day_Time DATETIME NOT NULL
);
GO

GO
CREATE TABLE User_Member_Details
(
Member_Name VARCHAR(50),
Owner_Email VARCHAR(50),
Member_Email VARCHAR(50),
BarrowedAmount VARCHAR(10),
GivenAmount VARCHAR(10),
Date_Day_Time DATETIME,
CONSTRAINT Compopsite_Pk_email PRIMARY KEY(Owner_Email,Member_Email)
);
GO

GO
CREATE TABLE Barrowed
(
Barrowed_Id VARCHAR(5) CONSTRAINT pk_bid PRIMARY KEY,
Email VARCHAR(50) CONSTRAINT fk_Members_Email FOREIGN KEY REFERENCES User_Member_Details(Member_Email),
Note VARCHAR(200),
BarAmount VARCHAR(9),
Date_Day_Time DATETIME
);
GO


GO
CREATE TABLE Transactions
(
Transaction_ID VARCHAR(10) CONSTRAINT pk_bid PRIMARY KEY,
Owner_Email VARCHAR(50),
Member_Email VARCHAR(50),
PersonName VARCHAR(50),
Note VARCHAR(200),
Given_Amount Bigint,
Barrowed_Amount Bigint,
Date_Day_Time DATETIME,
FOREIGN KEY(Owner_Email,Member_Email) REFERENCES User_Member_Details(Owner_Email,Member_Email)
);
GO


-- Insert Queries  --


INSERT INTO Account values('Palla BhanuChandra','U1000','pallabhanu@gmail.com','palla@1000',SYSDATETIME())


INSERT INTO User_Member_Details VALUES('Palla BhanuChandra','vipin@gmail.com','pallabhanu@gmail.com',SYSDATETIME())


--Select Queries for every table--
Use SplitWise

Select * from Account

Select * from User_Member_Details

Select * from	Given

Select * from Barrowed

Delete Account where Uname = 'bhanu'

--Delete queries

DROP TABLE Barrowed

DROP TABLE Transactions

DROP TABLE User_Member_Details
