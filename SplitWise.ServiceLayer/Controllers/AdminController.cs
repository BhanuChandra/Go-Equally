﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SplitWise.BusinessLayer;
using SplitWise.DataAccessLayer;
using SplitWise.DataAccessLayer.Models;

namespace SplitWise.ServiceLayer.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class AdminController : Controller
    {

        //Get All Admin Counts
        [HttpGet]
        public JsonResult GetAccounts()
        {
            DataAccess_Layer DAL = new DataAccess_Layer();
            List<Account> accounts = DAL.GetAccountDetails();
            return Json(accounts) ;
        }


        //Get Admin Wise Member Count
        [HttpPost]
        public JsonResult Get_User_Member_Count(UserMemberDetails getcountBY_email)
        {
            Business_Layer business_Layer = new Business_Layer();
            int memberCount = business_Layer.Get_User_Member_Count(getcountBY_email);
            return Json(memberCount);
        }
        
        //ADD Member
        [HttpPost]
        public JsonResult AddMember(UserMemberDetails addMember)
        {
            string status = "";
            try
            {
                Business_Layer business_Layer = new Business_Layer();
                status = business_Layer.AddMember(addMember);
            }
            catch
            {
                throw;
            }
            
            return Json(status);
        }

        //Register
        [HttpPost]
        public JsonResult Add_new_account(Account new_account)
        {
            bool status = false;
            Business_Layer business_Layer = new Business_Layer();

            //bool status = true;
            try
            {
                status = business_Layer.add_new_account(new_account);
            }
            catch (Exception)
            {
                status = false;
            }
            return Json(status);
        }

        //Login
        [HttpPost]
        public JsonResult UserLogin(Account Login_Account)
        {
            bool status = false;
            Business_Layer business_Layer = new Business_Layer();

            try
            {
                status = business_Layer.UserLogin(Login_Account);
            }
            catch (Exception)
            {
                status = false;
            }
            return Json(status);
        }

        //Get member by email
        [HttpPost]
        public JsonResult GetMyMembers(Account MAccount)
        {
            List<UserMemberDetails> members = new List<UserMemberDetails>();
            Business_Layer business_Layer = new Business_Layer();

            try
            {
                members = business_Layer.GetMyMembers(MAccount);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(members);
        }
        //Get All Transactions by owner email
        [HttpPost]
        public JsonResult MyTransactions(Account account)
        {
            List<Transactions> myTransactions = new List<Transactions>();
            Business_Layer business_Layer = new Business_Layer();

            try
            {
                myTransactions = business_Layer.MyTransactions(account);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(myTransactions);
        }
        //Add Transaction
        [HttpPost]
        public JsonResult AddTransactions(Transactions transactions)
        {
            bool status = false;
            Business_Layer business_Layer = new Business_Layer();

            try
            {
                status = business_Layer.AddTransactions(transactions);
            }
            catch (Exception)
            {
                status = false;
            }
            return Json(status);
        }

        //Delete member by email
        [HttpDelete]
        public JsonResult DeleteMember(UserMemberDetails delmember)
        {
            bool status = false;
            try
            {
                Business_Layer business_Layer = new Business_Layer();
                status = business_Layer.DeleteMember(delmember);
                
            }
            catch
            {
                status = false;
                throw;
            }
            return Json(status);
        }


    }

}