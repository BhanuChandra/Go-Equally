import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RegistrationComponent } from './Components/registration/registration.component';
import { SideNavigationComponent } from './Components/side-navigation/side-navigation.component';
import { LoginComponent } from './Components/login/login.component';
import { routing } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { MembersComponent } from './Components/members/members.component';
import { HomeSideNavigationComponent } from './Components/home-side-navigation/home-side-navigation.component';
import { AdminPanelComponent } from './Components/admin-panel/admin-panel.component';
import { AdminNavComponent } from './Components/admin-nav/admin-nav.component';
import { TransactionsComponent } from './Components/transactions/transactions.component';



@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    SideNavigationComponent,
    LoginComponent,
    MembersComponent,
    HomeSideNavigationComponent,
    AdminPanelComponent,
    AdminNavComponent,
    TransactionsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
