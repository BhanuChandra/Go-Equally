export interface IMembers {

  memberName: string;
  ownerEmail: string;
  memberEmail: string;
  givenAmount: string;
  barrowedAmount: string;
}
