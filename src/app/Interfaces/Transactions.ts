export interface Itransactions {
  transactionsId: string;
  personName: string;
  ownerEmail: string;
  memberEmail: string;
  barrowedAmount: number;
  givenAmount: number;
  note: string;
  dateDayTime: Date;
}
