import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-side-navigation',
  templateUrl: './side-navigation.component.html',
  styleUrls: ['./side-navigation.component.css']
})
export class SideNavigationComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  Login() {
    this._router.navigate(['/login']);
  }
  Registration() {
    this._router.navigate(['/registration'])
  }
  Home() {
    this._router.navigate([''])
  }
  Members(){
    this._router.navigate(['/members'])
  }
  Settings(){
    this._router.navigate([''])
  }






}
