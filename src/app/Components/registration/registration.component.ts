import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AdminService } from 'src/app/Services/admin.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {


  errMsg: string;
  showDiv: boolean = null;
  constructor(private _adminServices: AdminService) { }

  submitRegistrationForm(form: NgForm) {
    console.log(form.value.name);
    console.log(form.value.email);
    console.log(form.value.password);

    this._adminServices.UserRegistration(form.value.name, form.value.email, form.value.password).subscribe(
      responseAccountAdding => {
        if (responseAccountAdding) {
          this.showDiv = true;
          console.log("Registration Successful");
        }
        else {
          this.showDiv = false;
          console.log("Registration Failed");
        }
      },
      responseAccountaddingError => {
        this.errMsg = responseAccountaddingError;
        console.log(this.errMsg);
      }
    );

    
  }

  ngOnInit() {
  }

  
}
