import { Component, OnInit, PACKAGE_ROOT_URL, ViewChildren, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/Services/admin.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  adminEmail: string;
  displymsg: string;
  showDiv: boolean = null;
  memberCount: number = 0;

  constructor(private _AdminServices: AdminService, private _router: Router) {
    this.adminEmail = sessionStorage.getItem('userName');
    console.log(this.adminEmail);
    this.GetUserCount(this.adminEmail);
  }

  ngOnInit() {
  }


  GetUserCount(email: string) {
    this._AdminServices.GetMemberCount(email).subscribe(
      userCount => {
        if (userCount >= 0)
          this.memberCount = userCount
      }

    )
  }

  AddMember(form: NgForm) {
    
    this._AdminServices.AddMember(form.value.Member_Name, this.adminEmail, form.value.Member_Email).subscribe(
      responseAddMember => {
        if (responseAddMember == "Success") {
          this.showDiv = true;
          //$("#Add_Member").modal("hide");
          this._router.navigate(['/members']);
          
          
          this.displymsg = "Member Added"
        }
        else if (responseAddMember == "Failed") {
          this.showDiv = false;
          this.displymsg = "Sorry Failed to Add!!!!";
        }
        else if (responseAddMember == "Existed") {
          this.showDiv = false;
          this.displymsg = "Member already existed";
        }
        else {
          console.log(responseAddMember);
          this.showDiv = false;
          this.displymsg = "May be member you want to add is not a splitbill User!!";
        }
      }
    )
  }
  
}
