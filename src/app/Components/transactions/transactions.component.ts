import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/Services/admin.service';
import { Router } from '@angular/router';
import { Itransactions } from 'src/app/Interfaces/Transactions';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {


  Transactions: Itransactions[];

  constructor(private _http: AdminService, private _router: Router) { }

  ngOnInit() {
    this.getTransactions();
  }
  refresh() {
    this.ngOnInit();
  }

  getTransactions() {
    console.log("I am Transactions");
    this._http.MyTransactions(sessionStorage.getItem('userName')).subscribe(
      TransactionResponse => {
        this.Transactions = TransactionResponse
        console.log(this.Transactions[0].personName);
      }
    )
    //for (let i of this.Transactions) {
    //  console.log(i.PersonName);
    //}

  }
}
