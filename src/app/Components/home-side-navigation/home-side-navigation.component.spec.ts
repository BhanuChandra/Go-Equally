import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeSideNavigationComponent } from './home-side-navigation.component';

describe('HomeSideNavigationComponent', () => {
  let component: HomeSideNavigationComponent;
  let fixture: ComponentFixture<HomeSideNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeSideNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeSideNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
