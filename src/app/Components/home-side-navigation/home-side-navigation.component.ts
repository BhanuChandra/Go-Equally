import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-side-navigation',
  templateUrl: './home-side-navigation.component.html',
  styleUrls: ['./home-side-navigation.component.css']
})
export class HomeSideNavigationComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  Logout() {
    console.log("Not yet developed");
  }
  Home() {
    this._router.navigate([''])
  }
  Members() {
    this._router.navigate(['/members'])
  }
  Settings() {
    this._router.navigate([''])
  }

}
