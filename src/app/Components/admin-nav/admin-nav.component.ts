import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-nav',
  templateUrl: './admin-nav.component.html',
  styleUrls: ['./admin-nav.component.css']
})
export class AdminNavComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  Logout() {
    sessionStorage.clear();
    this._router.navigate(['/login']);
  }

  Members() {
    this._router.navigate(['/members']);
  }

  Transactions() {
    this._router.navigate(['/transactions']);
  }

  Dashboard() {
    this._router.navigate(['/AdminPanel']);

  }

}
