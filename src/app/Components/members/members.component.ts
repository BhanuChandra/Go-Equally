import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/Services/admin.service';
import { IMembers } from 'src/app/Interfaces/User_Members';
import { window } from 'rxjs/operators';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {

  adminMail: string
  Members: IMembers[];
  memberEmail: string;
  memberdeleteresponse: boolean;
  showDiv: boolean = null;
  constructor(private _http: AdminService) {
    this.adminMail = sessionStorage.getItem('userName')
  }

  ngOnInit() {
    
    this.GetMyMembers(this.adminMail);
  }

  GetMyMembers(adminMail: string) {
    
    this._http.GetMy_members(adminMail).subscribe(
      responseMembersList => {
        this.Members = responseMembersList
      }
    )
  }

  DeleteMember(memberEmail: string) {
    alert("Are you sure you want to delete " + memberEmail)
    this._http.DeleteMember(this.adminMail, memberEmail).subscribe(
      responsedeletemember => {
        
        if (responsedeletemember == true) {
          this.memberdeleteresponse = responsedeletemember
          console.log(this.memberdeleteresponse);
          this.ngOnInit();
        }
        else {
          this.memberdeleteresponse = responsedeletemember
        }
        
      }
    )
  }

  AddTransactionsMember(form: NgForm)
  {
    
    console.log(form.value.note);
    console.log(form.value.Amount);
    console.log(form.value.email);
    console.log(form.value.mname);
    console.log(form.value.Transtype);
    
    this._http.AddTransaction(form.value.mname, this.adminMail, form.value.email, form.value.Amount, form.value.note, form.value.Transtype).subscribe(
      responseAddTransaction => {
        if (responseAddTransaction == true) {
          this.showDiv = true;
          console.log("Added");
        }
        else {
          this.showDiv = false;
          
          console.log("Not able to add");
        }
      }
    )
    console.log(this.showDiv);


  }
  getTransactions(memberEmail: string, ownerEmail: string) {

  }

  

}
