import { Component, OnInit } from '@angular/core';
import { Router, Route } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AdminService } from 'src/app/Services/admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private _AdminServices: AdminService, private _router: Router) {
  }

  ngOnInit() {
  }

  Login(form: NgForm) {
    //console.log(form.value.useremail);
    //console.log(form.value.userpassword);
    this._AdminServices.UserLogin(form.value.useremail, form.value.userpassword).subscribe(
      (responseLogin = false) => {
        console.log(responseLogin);
        if (responseLogin) {
          //console.log("Login Successful");
          sessionStorage.setItem('userName', form.value.useremail);
          this._router.navigate(['/AdminPanel']);
        }
        else
          console.log("Login failed");
      }
    );
  }

}
