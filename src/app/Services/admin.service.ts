import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IAccount } from '../Interfaces/Account';
import { IMembers } from '../Interfaces/User_Members';
import { Itransactions } from '../Interfaces/Transactions';
import { getLocaleDateTimeFormat } from '@angular/common';


@Injectable({
  providedIn: 'root',
})
export class AdminService {

  constructor(private http: HttpClient) { }

  UserRegistration(name: string, email: string, password: string): Observable<boolean> {
    var accountObj: IAccount;
    accountObj = { Uname: name, Email: email, UserId: null, Password: password }
    return this.http.post<boolean>("https://localhost:44341/api/Admin/Add_new_account", accountObj).pipe(catchError(this.errorHandler))
  }

  UserLogin(email: string, password: string): Observable<boolean> {
    var accountObj: IAccount;
    accountObj = { Email: email, Password: password, Uname: null, UserId: null }
    return this.http.post<boolean>("https://localhost:44341/api/Admin/UserLogin", accountObj).pipe(catchError(this.errorHandler))
  }

  GetMemberCount(adminEmail: string): Observable<number> {
    var memberObj: IMembers;
    memberObj = { memberName: null, ownerEmail: adminEmail, memberEmail: null, givenAmount: "0", barrowedAmount: "0" };
    //var email = adminEmail;
    //let param1 = new HttpParams().set('Owner_Email', email)
    return this.http.post<number>("https://localhost:44341/api/Admin/Get_User_Member_Count", memberObj).pipe(catchError(this.errorHandler))
  }

  AddMember(Name: string, OwnerEmail: string, MemberEmail: string): Observable<string> {
    var memberObj: IMembers
    memberObj = { memberName: Name, memberEmail: MemberEmail, ownerEmail: OwnerEmail, givenAmount: null, barrowedAmount: null }
    return this.http.post<string>("https://localhost:44341/api/Admin/AddMember", memberObj).pipe(catchError(this.errorHandler));
  }

  //Get Member details by using mail

  GetMy_members(email: string): Observable<IMembers[]> {
    var account: IAccount
    account = { UserId: null, Email: email, Password: null, Uname: null }
    let tempVar = this.http.post<IMembers[]>('https://localhost:44341/api/Admin/GetMyMembers', account).pipe(catchError(this.errorHandler));
    return tempVar;
  }

  //Get Transaction details by using mail

  MyTransactions(email: string): Observable<Itransactions[]> {
    var account: IAccount
    account = { UserId: null, Email: email, Password: null, Uname: null }
    let tempVar = this.http.post<Itransactions[]>('https://localhost:44341/api/Admin/MyTransactions', account).pipe(catchError(this.errorHandler));
    return tempVar;
  }

  DeleteMember(adminmail: string, membermail: string): Observable<boolean> {
    var memberObj: IMembers
    memberObj = { memberName: null, memberEmail: membermail, ownerEmail: adminmail, givenAmount: null, barrowedAmount: null }
    let httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: memberObj };
    return this.http.delete<boolean>('https://localhost:44341/api/Admin/DeleteMember', httpOptions).pipe(catchError(this.errorHandler));
  }

  //Add Transaction

  AddTransaction(Name: string, OwnerEmail: string, MemberEmail: string, Amount: number, Note: string, TransactionType: string): Observable<boolean> {
    var transactionObj: Itransactions
    let date: Date = new Date();
    if (TransactionType == "Gave") {
      transactionObj = { transactionsId: null, ownerEmail: OwnerEmail, memberEmail: MemberEmail, personName: Name, note: Note, givenAmount: Amount, barrowedAmount: null, dateDayTime: date }
    }
    if (TransactionType == "Barrowed") {
      transactionObj = { transactionsId: null, ownerEmail: OwnerEmail, memberEmail: MemberEmail, personName: Name, note: Note, givenAmount: Amount, barrowedAmount: null, dateDayTime: date }
    }
    return this.http.post<boolean>("https://localhost:44341/api/Admin/AddTransactions", transactionObj).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    console.error(error);
    return throwError(error.message || "Server Error");
  }

}
