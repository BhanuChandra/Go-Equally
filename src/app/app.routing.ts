 import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { SideNavigationComponent } from './Components/side-navigation/side-navigation.component';
import { RegistrationComponent } from './Components/registration/registration.component';
import { LoginComponent } from './Components/login/login.component';
import { MembersComponent } from './Components/members/members.component';
import { AdminPanelComponent } from './Components/admin-panel/admin-panel.component';
import { AdminNavComponent } from './Components/admin-nav/admin-nav.component';
import { TransactionsComponent } from './Components/transactions/transactions.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'AdminPanel', component: AdminPanelComponent },
  { path: 'AdminNav', component: AdminNavComponent },
  { path: 'members', component: MembersComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'transactions', component: TransactionsComponent },
  { path: 'login', component: LoginComponent },
  { path: '**', component: LoginComponent },

];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
