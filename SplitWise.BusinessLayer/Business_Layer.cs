﻿using SplitWise.DataAccessLayer;
using SplitWise.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SplitWise.BusinessLayer
{
    public class Business_Layer
    {
        public bool add_new_account(Account new_account)
        {
            DataAccess_Layer DAL = new DataAccess_Layer();
            bool status = DAL.add_new_Account(new_account);
            return status;
        }

        public int Get_User_Member_Count(UserMemberDetails getcountBY_email)
        {
            DataAccess_Layer DAL = new DataAccess_Layer();
            int memberCount = DAL.Get_User_Member_Count(getcountBY_email);
            return memberCount;
        }

        public string AddMember(UserMemberDetails addMember)
        {
            string status = "";
            DataAccess_Layer DAL = new DataAccess_Layer();
            status = DAL.AddMember(addMember);
            return status;
        }

        public bool UserLogin(Account Login_Account)
        {
            DataAccess_Layer DAL = new DataAccess_Layer();
            bool status = DAL.userLogin(Login_Account);
            return status;
        }

        public List<Transactions> MyTransactions(Account account)
        {
            DataAccess_Layer DAL = new DataAccess_Layer();
            List<Transactions> myTransactions = new List<Transactions>();
            myTransactions = DAL.MyTransactions(account);
            return myTransactions;
        }

        public List<UserMemberDetails> GetMyMembers(Account account)
        {
            DataAccess_Layer DAL = new DataAccess_Layer();
            List<UserMemberDetails> myMembers = new List<UserMemberDetails>();
            myMembers = DAL.GetMyMembers(account);
            return myMembers;
        }

        public bool DeleteMember(UserMemberDetails delmember)
        {
            bool status=false;
            try
            {
                DataAccess_Layer DAL = new DataAccess_Layer();
                status=DAL.DeleteMember(delmember);
            }
            catch
            {
                status = false;
                throw;
            }
            return status;
        }

        public bool AddTransactions(Transactions transactions)
        {
            bool status = false;
            try
            {
                DataAccess_Layer DAL = new DataAccess_Layer();
                status = DAL.AddTransactions(transactions);
            }
            catch
            {
                status = false;
                throw;
            }
            return status;
        }

    }

}
